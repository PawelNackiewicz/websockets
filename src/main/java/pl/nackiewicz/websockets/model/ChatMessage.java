package pl.nackiewicz.websockets.model;

public class ChatMessage {

    private String value;
    private String user;

    public ChatMessage() {
    }

    public ChatMessage(String value, String user) {
        this.value = value;
        this.user = user;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
